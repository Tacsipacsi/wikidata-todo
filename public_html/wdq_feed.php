<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;

function prettyDate ( $ts ) {
	if ( $ts == '' ) return '' ;
	return substr($ts,0,4).'-'.substr($ts,4,2).'-'.substr($ts,6,2).' '.substr($ts,8,2).':'.substr($ts,10,2).':'.substr($ts,12,2) ;
}

function curPageURL() {
	return 'https://'.$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
}

function ts2ts ( $ts ) {
	return substr($ts,0,4).'-'.substr($ts,4,2).'-'.substr($ts,6,2).'T'.substr($ts,8,2).':'.substr($ts,10,2).':'.substr($ts,12,2)."Z" ;
}

function uuid ( $s ) {
	$s = preg_replace ( '/\D/' , 'a' , $s ) ;
	while ( strlen($s) < 32 ) $s .= '0' ;
	$s = substr($s,0,8).'-'.substr($s,8,4).'-'.substr($s,12,4).'-'.substr($s,16,4).'-'.substr($s,20,12) ;
	return "urn:uuid:$s" ;
}

function xmlentities($dirty){
$doc = new DOMDocument();
$fragment = $doc->createDocumentFragment();

// adding XML verbatim:

$fragment->appendChild($doc->createTextNode($dirty));

// output the result
return $doc->saveXML($fragment);
}



$ts_now = date ( "YmdHis" ) ;
$action = get_request ( 'action' , '' ) ;
$feed = get_request ( 'feed' , '0' ) * 1 ;
$try_feed = get_request ( 'try_feed' , '0' ) * 1 ;

$db = openToolDB ( 'feed_p' ) ;
$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;


if ( $action == 'add_feed' ) {
	header('Content-Type: text/plain'); 
	$user = str_replace ( '_' , ' ' , get_request ( 'user' , '' ) ) ;
	$name = get_request ( 'name' , '' ) ;
	$wdq = get_request ( 'wdq' , '' ) ;
	if ( $user == '' or $name == '' or $wdq == '' ) exit ( 0 ) ;
	$sql = "INSERT IGNORE INTO feed (name,wdq,creator,created) VALUES ('" . $db->real_escape_string($name) . "','" . $db->real_escape_string($wdq) . "','" . $db->real_escape_string($user) . "','$ts_now')" ;
	$out = array ( 'sql' => $sql , 'result' => 'OK' ) ;
	$result = getSQL ( $db , $sql ) ;
	else { # Run seed
		$feed = $db->insert_id;
		chdir ( '/data/project/wikidata-todo' ) ;
		system ( "./wdq_demon.php $feed" ) ;
	}
	
	print json_encode($out) ;
	exit ( 0 ) ;
}

if ( $feed != 0 ) {

	$title = '' ;
	$subtitle = '' ;
	$sql = "SELECT * FROM feed WHERE id=$feed" ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()) {
		$title = $o->name ;
		$subtitle = "feed by " . $o->creator . ", " . prettyDate($o->created) ;
		$subtitle .= "; query is: " . $o->wdq ;
	}
	if ( $title == '' ) {
		print get_common_header ( '' , 'WDQ atom feed' ) ;
		print "<div class='lead'>No such feed.</div>" ;
		print get_common_footer() ;
		exit ( 0 ) ;
	}
	
	$sql = "SELECT count(*) AS cnt FROM item_cache WHERE feed=$feed AND last_revision!=0" ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()) {
		$subtitle = $o->cnt . " total items in query result; $subtitle" ;
	}

#	header('Content-Type: text/plain'); 
	header('Content-Type: application/atom+xml'); 
	print "<?xml version='1.0' encoding='utf-8'?>
<feed xmlns='http://www.w3.org/2005/Atom'>
<title>" . xmlentities($title) . "</title>
<subtitle>" . xmlentities($subtitle) . "</subtitle>
<link href='" . xmlentities(curPageURL()) . "' rel='self' />
<link href='" . xmlentities('https://tools.wmflabs.org/wikidata-todo/wdq_feed.php') . "' />
<id>" . uuid("$feed-$ts_now") . "</id>
<updated>" . ts2ts($ts_now) . "</updated>" ;

	$max = get_request ( 'max' , '20' ) * 1 ;
	$lang = $db->real_escape_string ( get_request ( 'lang' , 'en' ) ) ;
	
	
	$items = array() ;
	$sql = "SELECT * FROM item_cache WHERE feed=$feed ORDER BY timestamp DESC LIMIT $max" ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()) {
		$o->label = 'Q'.$o->item ;
		$items[$o->item] = $o ;
	}
	
	if ( count($items) > 0 ) {
		$sql = "SELECT * FROM wb_terms WHERE term_entity_id IN (" . implode(',',array_keys($items)) . ") AND term_type='label' AND term_language='$lang'" ;
		$result = getSQL ( $dbwd , $sql ) ;
		while($o = $result->fetch_object()) {
			$items[$o->term_entity_id]->label = $o->term_text ;
		}
	}
	
	foreach ( $items AS $item => $i ) {
		print "<entry>\n" ;
		print "<title>" . xmlentities($i->label) . "</title>\n" ;
		print "<link href='https://www.wikidata.org/wiki/Q$item' />\n" ;
		print "<id>" . uuid("$item-" . $i->last_revision) . "</id>\n" ;
		print "<updated>" . ts2ts($i->timestamp) . "</updated>\n" ;
		print "<summary>" . xmlentities($i->event) . "</summary>\n" ;
		print "<author><name>Wikidata Query feed</name></author>\n" ;
		print "</entry>\n" ;
	}
	
	print "</feed>" ;

	exit ( 0 ) ;
}


$max_last_feeds = 20 ;

print get_common_header ( '' , 'WDQ atom feed' ) ;


if ( $try_feed != 0 ) {
	
	print "<div class='lead'>Create your feed</div>
<form method='get' class='form form-inline'>
<input type='hidden' name='feed' value='$try_feed' />
Number of last changes: <input type='number' name='max' value='20' /><br/>
Language for labels: <input type='text' name='lang' value='en' />
<input type='submit' value='Go to feed' class='btn btn-primary' />
</form>" ;
	
} else {

	$search = get_request ( 'search' , '' ) ;

	print "<div class='lead'>This tool generates ATOM feeds of WDQ queries, for both new items and item edits. Feeds are updated hourly.</div>" ;
	print "<div id='widar_note'></div>" ;
	print "<div><form class='form-inline' method='get'>" ;
	print "Search feed names: <input type='text' name='search' placeholder='One or more words' value='$search' /> " ;
	print "<input type='submit' value='Find feeds' class='btn btn-primary' />" ;
	print "</form></div>" ;
	print "<div><h2>The last $max_last_feeds created feeds</h2>" ;

	$where = array() ;
	$parts = explode ( ' ' , trim ( $search ) ) ;
	foreach ( $parts AS $p ) {
		if ( $p == '' ) continue ;
		$where[] = "name like '%" . $db->real_escape_string(trim($p)) . "%'" ;
	}

	
	if ( count($where) > 0 ) {
		print "<div>Searching for \"<tt>$search</tt>\".</div>" ;
		$where = " WHERE " . implode ( " AND " , $where ) . " " ;
	} else $where = '' ;

	print "<table class='table table-striped table-condensed'>" ;
	print "<thead><tr><th/><th>Feed name</th><th>Query</th><th>Creator</th><th>Last update</th></tr></thead><tbody>" ;
	
	$sql = "SELECT * FROM feed $where ORDER BY created DESC LIMIT $max_last_feeds" ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()){
		print "<tr>" ;
		print "<td><a href='?feed=" . $o->id . "' title='Click here for the default feed, or click the link on the right for a customized feed.'><img src='https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Generic_Feed-icon.svg/20px-Generic_Feed-icon.svg.png' border=0 /></a></td>" ;
		print "<th><a href='?try_feed=" . $o->id . "' title='Click here to construct your ATOM feed'>" . $o->name . "</a></th>" ;
		print "<td><a href='//tools.wmflabs.org/autolist/?wdq=".urlencode($o->wdq)."&run=Run' target='_blank' title='Open this query in AutoList'>" . $o->wdq . "</a></td>" ;
		print "<td><a href='//www.wikidata.org/wiki/".myurlencode($o->creator)."' target='_blank' title='Feed was created " . prettyDate($o->created) . "'>" . $o->creator . "</a></td>" ;
		print "<td>" . prettyDate ( $o->last_update ) . "</td>" ;
		print "</tr>" ;
	}
	print "</tbody></table>" ;
	print "</div>" ;

?>
<script>
var widar_api = '/widar/index.php?' ;
var oauth = {} ;

function checkOauthStatus () {
	$('#widar_note').html ( '<i>Checking WiDaR status...</i>' ) ;
	$.get ( widar_api , {
		action:'get_rights',
		botmode:1
	} , function ( d ) {
		var h = '' ;
		if ( d.error != 'OK' || typeof (d.result||{}).error != 'undefined' ) {
			h += "To add new feeds, you need to <a href='/widar/index.php?action=authorize' target='_blank'>authorise WiDaR</a>. <a href='#' onclick='checkOauthStatus();return false'>Check again.</a>" ;
		} else {
			oauth = d.result.query ;
			h += "<form class='form-inline' id='add_feed'>" ;
			h += "Welcome, <a target='_blank' href='//www.wikidata.org/wiki/User:" + encodeURIComponent(d.result.query.userinfo.name) + "'>" + d.result.query.userinfo.name + "</a>.<br/>" ;
			h += "Create a new feed: <input type='text' id='new_name' placeholder='Name your new feed' /> " ;
			h += "<input type='text' id='new_wdq' placeholder='Enter your Wikidata Query' /> " ;
			h += "<input type='submit' class='btn btn-primary' value='Create feed' /> " ;
			h += "<small><a href='http://wdq.wmflabs.org/api_documentation.html' target='_blank'>WDQ documentation</a></small>" ;
			h += "</form>" ;
		}
		$('#widar_note').html ( h ) ;
		$('#add_feed').submit ( function(e) {
			var name = $.trim($('#new_name').val()) ;
			var wdq = $.trim($('#new_wdq').val()) ;
			if ( name == '' || wdq == '' ) {
				alert ( "Please fill in both a feed name and a query" ) ;
				return false ;
			}
			$.get ( './wdq_feed.php' , {
				action:'add_feed',
				user:oauth.userinfo.name,
				name:name,
				wdq:wdq
			} , function ( d ) {
				if ( d.result != 'OK' ) {
					alert ( d.result ) ;
					return ;
				}
				alert ( "Your feed has been created, but will remain empty until the next update." ) ;
				document.location.reload(true);
			} , 'json' ) ;
			return false ;
		} ) ;
	} , 'json' ) ;
}

$(document).ready ( function () {
	checkOauthStatus() ;
} ) ;
</script>
<?PHP

}

print get_common_footer() ;

?>