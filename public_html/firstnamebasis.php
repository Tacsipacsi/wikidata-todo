<?PHP

header ( "text/plain" ) ;

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','6500M');
set_time_limit ( 60 * 60 ) ; // Seconds
include_once ( "php/common.php" ) ;

$minq = 10 ;

#print get_common_header ( '' , 'Sexer' ) ;
#print "<div>Getting all first names of people without gender on Wikidata... (this will take a minute or so)</div>" ; myflush() ;

$db = openDB ( 'wikidata' , 'wikidata' ) ;

function count_wdq ( &$j , $gender ) {
	global $db , $name_count ;
	$sql = "select term_full_entity_id,group_concat(DISTINCT term_text,CHAR(1) separator '|') AS label FROM wb_terms where  term_type='label' and term_entity_type='item' and term_full_entity_id in ('Q" . join("','Q",$j) . "') group by term_full_entity_id" ;
	unset ( $j ) ;

	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()){
		$a = explode ( '|' , $o->label ) ;
		foreach ( $a AS $name ) {
			$name = preg_replace ( '/_/' , ' ' , $name ) ;
			$name = preg_replace ( '/^death of /i' , '' , $name ) ;
			$name = preg_replace ( '/^lord /i' , '' , $name ) ;
			if ( preg_match ( '/\bsaint\b/' , $name ) ) continue ;
			if ( preg_match ( '/\b[au]nd\b/' , $name ) ) continue ;
			if ( preg_match ( '/\bMrs\.\b/' , $name ) ) continue ;
			if ( preg_match ( '/\bLady\b/' , $name ) ) continue ;
			if ( preg_match ( '/\bdiscography\b/i' , $name ) ) continue ;
			$name = explode ( ' ' , $name ) ;
			while ( count($name) > 1 ) {
				if ( preg_match ( '/\.$/' , $name[0] ) ) array_shift ( $name ) ;
				else break ;
			}
			if ( count ( $name ) < 2 ) continue ;
			$name_count[ucfirst($name[0])][$gender]++ ;
		}
	}

}

$name_count = array() ;
$j = getSPARQLitems ( 'SELECT ?q WHERE { ?q wdt:P31 wd:Q5 . ?q wdt:P21 wd:Q6581097 }' ) ;
count_wdq ( $j , 'male' ) ;
$j = getSPARQLitems ( 'SELECT ?q WHERE { ?q wdt:P31 wd:Q5 . ?q wdt:P21 wd:6581072 }' ) ;
count_wdq ( $j , 'female' ) ;

print "#Name\tMale\tFemale\n" ;
foreach ( $name_count AS $name => $d ) {
	$m = isset($d['male']) ? $d['male'] : 0 ;
	$f = isset($d['female']) ? $d['female'] : 0 ;
	if ( $m + $f < 100 ) continue ;
	print "$name\t$m\t$f\n" ;
}

#print get_common_footer() ;

?>