<?PHP

require_once ( 'php/common.php' ) ;

$num_result_artists = 10 ;

print get_common_header ( '' , 'Artwork images' ) ;

# Get artwork with creator but no image
$j1 = json_decode ( file_get_contents ( "$wdq_internal_url?q=" . urlencode('claim[170] and noclaim[18] and claim[31:(tree[838948][][279])]') . "&props=170,973" ) ) ;

$p170 = '170' ;
$creator_q2item = array() ;
foreach ( $j1->props->$p170 AS $v ) {
	$creator_q2item[''.$v[2]][] = $v[0] ;
}
$p973 = '973' ;
$item2url = array() ;
foreach ( $j1->props->$p973 AS $v ) {
	$item2url[''.$v[0]][] = $v[2] ;
}
unset ( $j1 ) ;
#print "<pre>" ; print_r ( $item2url ) ; print "</pre>" ;

# Get creators and their Creator pages on Commons
$j2 = json_decode ( file_get_contents ( "$wdq_internal_url?q=" . urlencode('claim[1472]') . "&props=1472" ) ) ;

$p1472 = '1472' ;
$creator2q = array() ;
foreach ( $j2->props->$p1472 AS $v ) {
	if ( !isset($creator_q2item[$v[0]]) ) continue ; // Not missing an image
	$creator2q[str_replace(' ','_',$v[2])] = $v[0] ;
}
unset ( $j2 ) ;

# Random subset
$keys = array_rand ( $creator2q , $num_result_artists ) ;
$tmp1 = array() ;
foreach ( $keys AS $k ) $tmp1[$k] = $creator2q[$k] ;
$creator2q = $tmp1 ;


$db = openDB ( 'commons' , 'commons' ) ;
$tmp1 = array() ;
foreach ( $creator2q AS $k => $v ) $tmp1[] = $db->real_escape_string ( $k ) ;
$creator2image = array() ;
$sql = "select DISTINCT page_title,pl_title from page,pagelinks,categorylinks where page_id=pl_from and pl_namespace=100 and pl_title IN ('" . implode("','",$tmp1) . "') and pl_from_namespace=6 and cl_from=page_id and cl_to='Artworks_without_Wikidata_item'" ;
$result = getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()){
	$creator2image[$o->pl_title][] = $o->page_title ;
}

$db = openDB ( 'wikidata' , 'wikidata' ) ;

$cnt = 0 ;
foreach ( $creator2image AS $creator => $images ) {
	$cnt++ ;
	print "<p><a href='#loc$cnt'>" . str_replace('_',' ',$creator) . "</a></p>" ;
}

$cnt = 0 ;
$thumb_width = 128 ;
$langs = array ( 'en' , 'de' , 'fr' , 'es' , 'it' , 'pl' , 'pt' , 'ja' , 'ru' , 'hu' ) ;
foreach ( $creator2image AS $creator => $images ) {
	$cnt++ ;
	print "<h3><a name='loc$cnt'></a>" . str_replace ( '_' , ' ' , $creator ) . "</h3>" ;
	
	$creator_q = '' . $creator2q[$creator] ;
	$iq = $creator_q2item[$creator_q] ;
	$image_q2labels = array() ;
	$sql = "select term_full_entity_id,term_language,term_text from wb_terms where term_entity_type='item' and term_type='label' AND term_full_entity_id IN ('Q" . implode("','Q",$iq) . "') AND term_language IN ('" . implode ( "','" , $langs ) . "')" ;
	$result = getSQL ( $db , $sql ) ;
	while($r = $result->fetch_object()){
		$image_q2labels[preg_replace('/\D/','',$r->term_full_entity_id)][$r->term_language] = $r->term_text ;
	}
	
//	if ( count($image_q2labels) == 0 ) continue ; // WTF?

	print "<h4>Items without image</h4>" ;
	print "<table class='table table-condensed table-striped'><tbody>" ;
	foreach ( $image_q2labels AS $image_q => $labels ) {
		foreach ( $langs AS $l ) {
			if ( !isset($labels[$l]) ) continue ;
			print "<tr><td><a href='//www.wikidata.org/wiki/Q$image_q' target='_blank'>Q$image_q</a></td><td style='width:100%'>" . $labels[$l] ;
			if ( isset($item2url[$image_q]) ) print "<br/><a href='" . $item2url[$image_q][0] . "' target='_blank'><i>described here</i></a>" ;
			print "</td></tr>" ;
			break ;
		}
	}
	print "</tbody></table>" ;
	
	
	print "<h4>Images without items</h4>" ;
	print "<table class='table table-condensed table-striped'><tbody>" ;
	foreach ( $images AS $image ) {
		$url = get_thumbnail_url ( 'commons' , $image , $thumb_width , 'wikimedia' ) ;
		print "<tr>" ;
		print "<td><a href='//commons.wikimedia.org/wiki/File:" . str_replace("'","&39;",myurlencode($image)) . "' target='_blank'><img src='" . $url . "' border='0px' /></a></td>" ;
		print "<td style='width:100%'><pre>" . str_replace('_',' ',$image) . "</pre></td>" ;
		print "</tr>" ;
	}
	print "</tbody></table>" ;
}

#print "<pre>" ; print_r ( $creator2image ) ; print "</pre>" ;

print get_common_footer() ;

?>