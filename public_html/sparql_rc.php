<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

ini_set('memory_limit','1500M');
set_time_limit ( 60 * 10 ) ; // Seconds

require_once ( 'php/ToolforgeCommon.php' ) ;

// This will resolve direct timestamps, as well as text such as "last week"
function fixDate ( &$d ) {
	$d = trim ( $d ) ;
	if ( preg_match ( '/^\d+$/' , $d ) ) { // All numeric, all good, fillerup
		while ( strlen($d) < 14 ) $d .= '0' ;
		return ;
	}
	$nt = strtotime ( $d ) ;
	if ( $nt === false ) { // Don't know
		$d = preg_replace ( '/\D/' , '' , $d ) ;
	} else {
		$d = date ( "YmdHis" , $nt ) ;
	}
}

// This will fix the HTML of diffs, as in, proper links
function fixHTML ( $html ) {
	$html = preg_replace ( '/ href="(\/wiki\/.+?)"/' , 'href="https://www.wikidata.org$1"' , $html ) ;
	$html = preg_replace ( '/<a /' , '<a target="_blank" ' , $html ) ;
	return $html ;
}


// Called by uasort() to sort diff'ed items
function sort_callback ( $a , $b ) {
	global $sort_mode , $label_cache ;
	if ( $sort_mode == 'last_edit' ) {
		if ( $a[1]->rev_timestamp > $b[1]->rev_timestamp ) return -1 ;
		if ( $a[1]->rev_timestamp < $b[1]->rev_timestamp ) return 1 ;
		return 0 ;
	}
	if ( $sort_mode == 'label' ) {
		$q1 = $a[2]->page_title ;
		$q2 = $b[2]->page_title ;
		$l1 = isset($label_cache[$q1]) ? $label_cache[$q1] : $q1 ;
		$l2 = isset($label_cache[$q2]) ? $label_cache[$q2] : $q2 ;
		if ( $l1 < $l2 ) return -1 ;
		if ( $l1 > $l2 ) return 1 ;
		return 0 ;
	}
	die ( "Bad sort mode: $sort_mode" ) ;
}

// Returns nicely formatted timestamp
function prettyTS ( $ts ) {
	return substr($ts,0,4).'-'.substr($ts,4,2).'-'.substr($ts,6,2).'&nbsp;'.substr($ts,8,2).':'.substr($ts,10,2).':'.substr($ts,12,2) ;
}

// Renders a link to a user, IP or logged-in
function getUserLink ( $user_id , $user_name ) {
	global $tfc ;
	$base_url = $user_id==0 ? 'https://www.wikidata.org/wiki/Special:Contributions/' : 'https://www.wikidata.org/wiki/User:' ;
	return "<a href='" . $base_url . $tfc->urlEncode($user_name) . "' target='_blank'>$user_name</a>" ;
}


$tfc = new ToolforgeCommon ( 'sparql_rc' ) ;


// Get URL parameters
$sparql = $tfc->getRequest ( 'sparql' , '' ) ;
$start_date = $tfc->getRequest ( 'start' , '' ) ;
$end_date = $tfc->getRequest ( 'end' , '' ) ;
$no_bots = $tfc->getRequest ( 'no_bots' , 0 ) * 1 ;
$skip_unchanged = $tfc->getRequest ( 'skip_unchanged' , 0 ) * 1 ;
$sort_mode = $tfc->getRequest ( 'sort_mode' , 'last_edit' ) ;
$user_lang = $tfc->getRequest ( 'user_lang' , '' ) ;

// Set up languages
$languages = ['en','de','fr','es','it','nl','ru','zh'] ;
$ul = explode(',',$user_lang) ;
if ( count($ul) > 0 ) {
	$db = $tfc->openDB ( 'wikidata' , 'wikidata' ) ;
	while ( count($ul) > 0 ) {
		$l = array_pop ( $ul ) ;
		$l = $db->real_escape_string(strtolower(trim($l))) ;
		if ( $l == '' ) continue ;
		$languages = array_diff ( $languages , [$l] ) ; // Remove from list, if exists
		array_unshift ( $languages , $l ) ; // Put language first
	}
//	print "<pre>" ; print_r ( $languages ) ; print "</pre>" ;
}

// Print header and form
print $tfc->getCommonHeader ( '' , 'Wikidata SPARQL Recent Changes' ) ;

$dph = 'YYYYMMDDHHMMSS; shorter OK' ; // Date placeholder
print "
<div class='bd-title'>
  <p class='bd-lead'>
    Get diffs for all items matching a SPARQL query, for a date range.<br/>
    Examples: <a href='?sparql=SELECT+%3Fq+%7B+%3Fq+wdt%3AP31+wd%3AQ5+%3B+wdt%3AP106%2Fwdt%3AP279*+wd%3AQ1028181+%3B+wdt%3AP27+wd%3AQ142+%7D&start=20171001&end=20171007235959&no_bots=1&skip_unchanged=1'>French painters, first week of October 2017, no bots edits</a>,
    same for <a href='https://tools.wmflabs.org/wikidata-todo/sparql_rc.php?sparql=SELECT+%3Fq+%7B+%3Fq+wdt%3AP31+wd%3AQ5+%3B+wdt%3AP106%2Fwdt%3AP279*+wd%3AQ1028181+%3B+wdt%3AP27+wd%3AQ142+%7D&start=last+week&end=&no_bots=1&skip_unchanged=1'>last week</a>,
    or <a href='https://tools.wmflabs.org/wikidata-todo/sparql_rc.php?sparql=SELECT+%3Fq+%7B+%3Fq+wdt%3AP31+wd%3AQ5+%3B+wdt%3AP106%2Fwdt%3AP279*+wd%3AQ1028181+%3B+wdt%3AP27+wd%3AQ142+%7D&start=yesterday&end=&no_bots=1&skip_unchanged=1'>since yesterday</a>.
  </p>
</div>

<form method='get' class='form'>

<div class='form-group row'>
<label for='sparql' class='col-sm-2 col-form-label'>SPARQL query</label>
<div class='col-sm-10' style='line-height:0.5'>
<textarea name='sparql' class='form-control' placeholder='A SPARQL query; first variable needs to be named \"?q\" and contain item IDs' rows=3>$sparql</textarea>
<br/><small class='form-text text-muted'>Design your query <a href='https://query.wikidata.org' target='_blank'>here</a>.</small>
</div>
</div>

<div class='form-group row'>
<label for='start' class='col-sm-2 col-form-label'>Start date</label>
<div class='col-sm-10' style='line-height:0.5'>
<input class='form-control form-control-lg' type='text' placeholder='$dph' name='start' value='$start_date' /><br/>
<small class='form-text text-muted'>Alternatively, use a text like \"last week\" or such, as described <a href='https://secure.php.net/manual/en/datetime.formats.relative.php' target='_blank'>here</a>.</small>
</div>
</div>

<div class='form-group row'>
<label for='start' class='col-sm-2 col-form-label'>End date</label>
<div class='col-sm-10' style='line-height:0.5'>
<input class='form-control form-control-lg' type='text' placeholder='$dph' name='end' value='$end_date'></input><br/>
<small class='form-text text-muted'>
Alternatively, use a text like \"last week\" or such, as described <a href='https://secure.php.net/manual/en/datetime.formats.relative.php' target='_blank'>here</a>.
Leave empty for current date.</small>
</div>
</div>

<div class='form-group row'>
<label class='col-sm-2 col-form-label'>Preferred languages<br/>for item labels</label>
<div class='col-sm-10'>
<input type='text' name='user_lang' value='$user_lang' placeholder='language codes, separated by comma, preferred first; e.g. de,en,fr' />
</div>
</div>

<div class='form-group row'>
<label class='col-sm-2 col-form-label'>Sort order</label>
<div class='col-sm-10'>
<label><input type='radio' name='sort_mode' value='last_edit' " . ($sort_mode=='last_edit'?'checked':'') . "/> By last edit (latest first)</label>
<label><input type='radio' name='sort_mode' value='label' " . ($sort_mode=='label'?'checked':'') . "/> By label</label>
</div>
</div>

<div class='form-group row'>
<label class='col-sm-2 col-form-label'>Options</label>
<div class='col-sm-10'>
<label><input type='checkbox' name='no_bots' value='1' " . ($no_bots?'checked':'') . "/> No bot edits</label>
<label><input type='checkbox' name='skip_unchanged' value='1' " . ($skip_unchanged?'checked':'') . "/> Skip unchanged items</label>
</div>
</div>

<div class='form-group'>
<input type='submit' class='btn btn-primary' value='Get Recent Changes for items'></input>
</div>

</form>

<link rel='stylesheet' type='text/css' href='https://www.wikidata.org/w/load.php?debug=true&lang=en&modules=mediawiki.diff.styles&only=styles&skin=vector'>


<style>
h4.has_diff {
	border-top:3px solid #999;
	margin-top:10px;
	padding-top:5px;
}
h4.wb-details {
	font-size:1rem;
}
table.diff {
	font-size:0.8rem;
}
table.wb-details td {
	padding:1px;
}
div.editors {
	font-size:0.8rem;
	border-top:1px dotted #BBB;
	margin-top:5px;
}
span.ip_editor {
	background-color:#FF7575;
}
span.editor {
	padding-right:5px;
	padding-left:5px;
	border-left:1px solid #BBB;
}
span.editor:first-of-type {
	margin-left:0px;
	padding-left:0px;
	border-left:none;
}
div.edit_summary {
	padding-left:5px;
	border-left:10px solid #EEE;
	font-family:Courier;
	font-size:8pt;
}
</style>" ;


if ( $sparql == '' or $start_date == '' ) { // No query run
	print $tfc->getCommonFooter() ;
	exit ( 0 ) ;
}

// Prepare dates
if ( $end_date == '' ) $end_date = date ( "YmdHis" ) ;
fixDate ( $start_date ) ;
fixDate ( $end_date ) ;
if ( trim($start_date) == '' ) {
	print "<p>Invalid start date. Please use a date format described <a href='https://secure.php.net/manual/en/datetime.formats.relative.php' target='_blank'>here</a></p>" ;
	print $tfc->getCommonFooter() ;
	exit ( 0 ) ;
}

// Run SPARQL
if ( preg_match ( '/^\s*select\s+\?(\S+)/i' , $sparql , $m ) ) $varname = $m[1] ;
else $varname = 'q' ;
$items = $tfc->getSPARQLitems ( $sparql , $varname ) ;
if ( !isset($items) or $items == null ) {
	print "<p>SPARQL query <pre>$sparql</pre> has failed</p>" ;
	print $tfc->getCommonFooter() ;
	exit ( 0 ) ;
}
if ( count($items) == 0 ) {
	print "<p>SPARQL query <pre>$sparql</pre> has returned no results</p>" ;
	print $tfc->getCommonFooter() ;
	exit ( 0 ) ;
}

$items = array_unique ( $items ) ;

// Get revisions for these items, in the given time interval
$db = $tfc->openDB ( 'wikidata' , 'wikidata' ) ;

$no_change = [] ;
$q2rev = [] ;

$sql = "SELECT DISTINCT page_id FROM page WHERE page_namespace=0 AND page_title IN ('" . implode ( "','" , $items ) . "')" ;
$sql = "SELECT * FROM revision WHERE rev_page IN ($sql) AND rev_timestamp BETWEEN '$start_date' AND '$end_date'" ;
if ( $no_bots ) $sql .= " AND rev_user NOT IN (select ug_user from user_groups where ug_group='bot')" ;
//print "<pre>$sql</pre>" ;
$result = $tfc->getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()){
//	print "<pre>" ; print_r ( $o ) ; print "</pre>" ;
	$u = "{$o->rev_user}|{$o->rev_user_text}" ;
	if ( !isset($q2rev[''.$o->rev_page]) ) {
		$q2rev[''.$o->rev_page] = [ $o , $o , '' , [ $u => 1 ] ] ;
		continue ;
	}
	if ( $q2rev[''.$o->rev_page][0]->rev_timestamp > $o->rev_timestamp ) $q2rev[''.$o->rev_page][0] = $o ;
	if ( $q2rev[''.$o->rev_page][1]->rev_timestamp < $o->rev_timestamp ) $q2rev[''.$o->rev_page][1] = $o ;
	if ( !isset($q2rev[''.$o->rev_page][3][$u]) ) $q2rev[''.$o->rev_page][3][$u] = 1 ;
	else $q2rev[''.$o->rev_page][3][$u]++ ;
}

// Get page IDs for the items
$sql = "SELECT * FROM page WHERE page_namespace=0 AND page_title IN ('" . implode ( "','" , $items ) . "')" ;
$result = $tfc->getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()){
	if ( isset($q2rev[''.$o->page_id]) ) $q2rev[''.$o->page_id][2] = $o ;
	else $no_change[] = $o ;
}
print "<hr/><h3>Total number of items : " . count($items) . "</h3>" ;

// Cache labels for prioritized languages
$label_cache = [] ;
$itl = [] ; // Items to load
foreach ( $items AS $q ) $itl["$q"] = "$q" ;
foreach ( $languages AS $l ) {
	$sql = "SELECT term_full_entity_id,term_text FROM wb_terms WHERE term_entity_type='item' AND term_language='$l' AND term_type='label' AND term_full_entity_id IN ('" . implode ( "','" , $itl ) . "')" ;
	$result = $tfc->getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()){
		unset ( $itl[$o->term_full_entity_id] ) ;
		if ( !isset($label_cache[$o->term_full_entity_id]) ) $label_cache[$o->term_full_entity_id] = $o->term_text ;
	}
	if ( count($itl) == 0 ) break ;
}

// Show diffs
if ( count($q2rev) > 0 ) {
	uasort ( $q2rev , 'sort_callback' ) ;
	$star = '*' ;
	print "<h3>" . count($q2rev) . " items have changed between<br/><tt>" . prettyTS($start_date) . "</tt> and <tt>" . prettyTS($end_date) . "</tt></h3>" ;
	$items_loaded = [] ;
	foreach ( $q2rev AS $v ) $items_loaded[] = $v[2]->page_title ;
	
	// Prepare to load diffs via API in parallel
	$url_batch = [] ;
	foreach ( $q2rev AS $k => $v ) {
		$oldid = ($v[0]->rev_parent_id==0) ? $v[0]->rev_id : $v[0]->rev_parent_id ;
		$url = "https://www.wikidata.org/w/api.php?action=compare&fromrev=$oldid&torev={$v[1]->rev_id}&format=json" ;
		$url_batch[$k] = $url ;
	}
	
	$results = $tfc->getMultipleURLsInParallel ( $url_batch , 50 ) ; // Load up to 50 diff URLs in parallel; the API seems to handle it well

	// Print per-item diffs
	foreach ( $q2rev AS $k => $v ) {
		$q = $v[2]->page_title ;
		$data = $results[$k] ;
		if ( !isset($data) ) { print "<p>Diff loading failed for $q</p>" ; continue ; }
		$j = @json_decode ( $data ) ;
		if ( !isset($j) or $j == null ) { print "<p>Diff format compromised for $q</p>" ; continue ; }
		
		$oldid = ($v[0]->rev_parent_id==0) ? $v[0]->rev_id : $v[0]->rev_parent_id ;
		$url = "https://www.wikidata.org/w/index.php?title=$q&type=revision&diff={$v[1]->rev_id}&oldid=$oldid" ;

		print "<h4 class='has_diff'>" ;
		print "<a href='https://www.wikidata.org/wiki/{$v[2]->page_title}' target='_blank'>" ;
		print ( isset($label_cache[$q]) ? $label_cache[$q] : $q ) ;
		print "</a>" ;
		if ( isset($label_cache[$q]) ) print " <small>[$q]</small>" ;
		print " <small>[<a href='$url' target='_blank'>diff on wiki</a>]</small>" ;
		if ( $v[0]->rev_timestamp == $v[1]->rev_timestamp ) print " <small>" . prettyTS($v[0]->rev_timestamp) . "</small>" ;
		else print " <small>" . prettyTS($v[0]->rev_timestamp) . " &ndash; " . prettyTS($v[1]->rev_timestamp) . "</small>" ;
		print "</h4>" ;
	
		if ( !isset($j->compare) ) {
			print "<pre>" ; print_r ( $v ) ; print "</pre>" ;
		} else if ( $j->compare->$star == '' ) {
			$msg = htmlentities($v[1]->rev_comment) ;
			$msg = preg_replace ( '/^(Reverted edits) /' , '<span style="color:red">$1</span> ' , $msg ) ;
			print "<p><i>No change to item; last edit summary was:</i><br/><div class='edit_summary'>" ;
			print getUserLink ( $v[1]->rev_user , $v[1]->rev_user_text ) ;
			print ": $msg</div></p>" ;
		} else {
			if ( $v[0]->rev_parent_id == 0 ) {
				print "<p><div class='edit_summary'><span style='color:green'>The item was created!</span></div></p>" ;
			}
			print "<table class='diff diff-contentalign-left'>" ;
			print '<colgroup><col class="diff-marker"><col class="diff-content"><col class="diff-marker"><col class="diff-content"></colgroup>' ;
			print "<tbody>" ;
			print fixHTML ( $j->compare->$star ) ;
			print "</tbody></table>" ;
		}
		
		asort ( $v[3] , SORT_NUMERIC ) ;
		print "<div class='editors'>Editors: " ;
		foreach ( $v[3] AS $u => $cnt ) {
			$u = explode ( '|' , $u , 2 ) ;
			print "<span class='editor" . ($u[0]==0?' ip_editor':'') . "'>" ;
			print getUserLink ( $u[0] , $u[1] ) ;
			if ( $cnt > 1 ) print " ($cnt&times;)" ;
			print "</span>" ;
		}
		print "</div>" ;
	}
}

// Show unchanged items
if ( count($no_change) > 0 and !$skip_unchanged ) {
	print "<h3>" . count($no_change) . " items have not changed (as per parameters used)</h3>" ;
	print "<ol class='list-inline'>" ;
	foreach ( $no_change AS $k => $v ) {
		$q = $v->page_title ;
		print "<li class='list-inline-item'>" ;
		if ( $k > 0 ) print ' | ' ;
		print "<a href='https://www.wikidata.org/wiki/{$v->page_title}' target='_blank'>" ;
		print ( isset($label_cache[$q]) ? $label_cache[$q] : $q ) ;
		print "</a></li>" ;
	}
	print "</ol>" ;
}

// Aaaaand we're out
print $tfc->getCommonFooter() ;

?>