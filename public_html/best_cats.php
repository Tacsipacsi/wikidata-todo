<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

ini_set('memory_limit','1500M');
set_time_limit ( 60 * 10 ) ; // Seconds

include_once ( "php/common.php") ;

print get_common_header('','Best Cats') ;

$wiki = get_request ( 'wiki' , 'dewiki' ) ;
//$query = 'link[enwiki] and link[dewiki] and link[frwiki] and link[itwiki] and link[eswiki] and noclaim[31,279]' ;
$query = "link[$wiki] and noclaim[31,279]" ;
$url = "$wdq_internal_url?q=" . urlencode ( $query ) ;
print "<pre>$url</pre>" ;
$j = json_decode ( file_get_contents ( $url ) ) ;
if ( !isset($j->items) ) {
	print "<pre>" ; print_r ( $j ) ; print "</pre>" ;
}

$items = array() ;
$max = 500000 ;
if ( count ( $j->items ) < $max ) {
	$items = $j->items ;
} else {
	print "<div>This uses only a subset ($max of " . count($j->items) . ") of items; item numbers will be higher.</div><hr/>" ;
	shuffle ( $j->items ) ;
	$items = array_slice ( $j->items , 0 , $max ) ; // Random subset
}
unset ( $j ) ;

$titles = array() ;
$db = openDB ( 'wikidata' , '' ) ;
$sql = "SELECT ips_site_page FROM wb_items_per_site WHERE ips_site_id='$wiki' AND ips_item_id IN (" . implode(',',$items) . ")" ;
//print "<pre>$sql</pre>" ; myflush();
$result = getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()){
	$titles[] = $db->real_escape_string ( str_replace(' ','_',$o->ips_site_page) ) ;
}
unset ( $items ) ;

//print count ( $titles ) ;

print "<table class='table table-striped'>" ;
print "<thead><tr><th>Category</th><th>#</th></thead><tbody>" ;

$db = openDBwiki ( $wiki ) ;
$sql = "select cl_to,count(*) AS cnt from page,categorylinks where page_title IN ('" . implode("','",$titles) . "') and page_namespace=0 and page_id=cl_from and cl_to not like '%rticles%' and cl_to not like 'Pages%' AND cl_to NOT LIKE '%dmy%' AND cl_to NOT LIKE '%Commons%'  AND cl_to NOT LIKE 'All%' AND cl_to NOT LIKE '%Coordinates%'  AND cl_to NOT LIKE '%Wikipedia%' group by cl_to order by cnt desc limit 100" ;
//print "<pre>$sql</pre>" ; myflush();
$result = getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()){
	print "<tr>" ;
	print "<td>" . str_replace ( '_' , ' ' , $o->cl_to ) . "</td>" ;
	print "<td>" . $o->cnt . "</td>" ;
	print "</tr>" ;
}

print "</tbody></table>" ;

?>