<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // |E_ALL
ini_set('display_errors', 'On');

ini_set('user_agent','Magnus labs tools'); # Fake user agent
header("Connection: close");
header('Content-type: text/plain; charset=utf-8');
header("Cache-Control: no-cache, must-revalidate");

// Template names need to (1) be lowercase, except first letter uppercase (2) all "_" replaced by " "

$license_templates = array (
	'Cc-by-sa-2.0-de' => array ( 'attr_key' => '1' , 'q' => 'Q6905942' , 'version' => '2.0' , 'lang' => 'de' ) ,
) ;


$ignore_templates = array (
	'Int:license-header' ,
	'Original upload log'
) ;




$out = array ( 'problem' => array() , 'data' => array() ) ;
$file = $_REQUEST['file'] ;

$url = "http://commons.wikimedia.org/w/api.php?action=visualeditor&format=json&paction=parse&page=File:" . urlencode(str_replace(' ','_',$file)) ;
$j = json_decode ( file_get_contents ( $url ) ) ;
$src = $j->visualeditor->content ;

function deWiki ( $s ) { // Turns Wikitext into plain text
	$url = "http://commons.wikimedia.org/w/api.php?action=parse&format=json&prop=text&text=" . urlencode($s) ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	$star = '*' ;
	$ret = $j->parse->text->$star ;
	return trim ( strip_tags ( $ret ) ) ;
}

function parseTemplateElements ( $e , &$ret ) {
	if ( $e->hasAttributes() ) {
		$n = $e->attributes->getNamedItem('data-mw') ;
		if ( $n != NULL ) {
			$j = json_decode ( $n->value ) ;
			if ( isset ( $j->parts ) ) {
				foreach ( $j->parts AS $part ) {
					if ( isset ( $part->template ) ) {
						$ret[] = $part->template ;
					}
				}
			}
		}
	}

	if ( $e->hasChildNodes() ) {
		for ( $i = 0 ; $i < $e->childNodes->length ; $i++ ) {
			parseTemplateElements ( $e->childNodes->item($i) , $ret ) ;
		}
	}
}

function getTemplates ( $src ) {
	$doc = new DOMDocument();
	$doc->loadHTML($src);
	$ret = array() ;
	parseTemplateElements ( $doc->documentElement , $ret ) ;
	return $ret ;
}



function addInformationParam ( $key , $owt ) {
	global $out , $file ;
	
	$wt = trim ( $owt ) ;
	
	if ( $key == 'date' ) {
		if ( preg_match ( '/^(\d{3,4})-(\d{1,2})-(\d{1,2})$/' , $wt , $m ) ) {
			$d = array() ;
			$d['year'] = $m[1]*1 ;
			$d['month'] = $m[2]*1 ;
			$d['day'] = $m[3]*1 ;
			$out['data']['date'][] = $d ;
		} else if ( preg_match ( '/^(\d{3,4})-(\d{1,2})$/' , $wt , $m ) ) {
			$d = array() ;
			$d['year'] = $m[1]*1 ;
			$d['month'] = $m[2]*1 ;
			$out['data']['date'][] = $d ;
		} else if ( preg_match ( '/^(\d{3,4})$/' , $wt , $m ) ) {
			$d = array() ;
			$d['year'] = $m[1]*1 ;
			$out['data']['date'][] = $d ;
		} else {
			$out['problem']['date'][] = $owt ;
		}
	}
	
	if ( $key == 'source' ) {
		$self_made = array ( 'self-made' , 'selbst fotografiert' ) ;
		if ( in_array ( strtolower($wt) , $self_made ) ) {
			$out['data']['made_by_uploader'] = 1 ;
		} else {
			$out['problem']['source'][] = $owt ;
		}
	}
	
	if ( $key == 'description' and $wt != '' ) {
	
		// Now parsing language templates in $wt. Can't figure out how to parse
		// arbitary Wikitext in Parsoid, so using regexp. FML.
		$num = preg_match_all ( '/\{\{([a-z-]+)\|\d+=(.+?)\}\}/i' , $owt , $m ) ;
		if ( $num ) {
			for ( $n = 0 ; $n < $num ; $n++ ) {
				$lang = $m[1][$n] ;
				$text = $m[2][$n] ;
				$out['data']['description'][] = array ( 'language' => $lang , 'text' => deWiki ( $text ) ) ;
			}
		} else {// No templates
			$out['data']['description'][] = array ( 'language' => 'en' , 'text' => deWiki ( $owt ) ) ; // Default: en
		}
	}


	if ( $key == 'author' ) {
		if ( preg_match ( '/^\[\[:([a-z-]+):[Uu]ser:(.+?)\|(.+?)\]\]$/' , $wt , $m ) ) {
			if ( trim(strtolower($m[2])) != trim(strtolower($m[3])) ) {
				$out['problem']['author'][] = $owt ;
			} else {
				$out['data']['author'][] = array (
					'user_name' => $m[2] ,
					'source_type' => 'wikimedia' ,
					'wiki' => $m[1]
				) ;
			}
		} else {
			$out['problem']['author'][] = $owt ;
		}
	}
	
}


$templates = getTemplates ( $src ) ;
foreach ( $templates AS $t ) {
	if ( !isset ( $t->target->wt ) ) continue ;
	$wt = ucfirst ( strtolower ( trim ( str_replace ( '_' , ' ' , $t->target->wt ) ) ) ) ;
	if ( $wt == 'Information' ) {
		$keys = array ( 'Description' , 'Source' , 'Date' , 'Author' , 'Permission' , 'other_versions' ) ;
		foreach ( $keys AS $k ) {
			if ( isset($t->params->$k) ) addInformationParam ( strtolower($k) , $t->params->$k->wt ) ;
		}
	} else if ( isset ( $license_templates[$wt] ) ) {
		$lt =  $license_templates[$wt] ;
		if ( isset ( $lt['attr_key'] ) ) {
			$ak = $lt['attr_key'] ;
			unset ( $lt['attr_key'] ) ;
			if ( isset ( $t->params->$ak ) ) {
				$lt['attribution_text'] = deWiki ( $t->params->$ak->wt ) ;
			}
		}
		$out['data']['license'][] = $lt ;
	} else if ( in_array ( $wt , $ignore_templates ) ) { // Ignore
	} else {
		$out['problem']['template'][] = json_encode($t) ;
	}
}



if ( 0 ) { // Testing
//	print "\n\n----------\n\n" ;
	print_r ($templates) ;
//	print "\n\n----------\n\n" ;
	print_r ( $out ) ;
	exit ( 0 ) ;
}

if ( isset($_REQUEST['callback']) ) print $_REQUEST['callback']."(" ;
print json_encode($out) ;
if ( isset($_REQUEST['callback']) ) print ")" ;

?>