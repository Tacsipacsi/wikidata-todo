<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;
require_once ( 'php/ToolforgeCommon.php' ) ;
require_once ( '/data/project/pagepile/public_html/pagepile.php' ) ;

$tfc = new ToolforgeCommon('linked_items') ;
$language = get_request ( 'language' , 'en' ) ;
$project = get_request ( 'project' , 'wikipedia' ) ;
$page = trim ( get_request ( 'page' , '' ) ) ;
$pages = trim ( get_request ( 'pages' , '' ) ) ;
$pagepile = trim ( get_request ( 'pagepile' , '' ) ) ;
$wikitext = trim ( get_request ( 'wikitext' , '' ) ) ;
$format = trim ( get_request ( 'format' , 'screen' ) ) ;

function runQuery ( $url , $data ) {
	global $links1 , $links , $db , $tfc ;
	$d = json_decode ( $tfc->doPostRequest ( $url , $data ) ) ;

	if ( !isset($d->parse) or !isset($d->parse->links) ) {
		print "<div class='lead'>Something's wrong!</div>" ;
		print "<pre>" ; print_r ( $data ) ; print "</pre>" ;
		print get_common_footer() ;
		exit ( 0 ) ;
	}

	$star = '*' ;
	foreach ( $d->parse->links AS $l ) {
		if ( $l->ns != 0 ) continue ;
		if ( !isset($l->exists) ) continue ;
		$t1 = str_replace ( ' ' , '_' , $l->$star ) ;
		$t2 = str_replace ( '_' , ' ' , $l->$star ) ;
		$links1[] = $db->real_escape_string ( $t1 ) ;
		$links[$t1] = $db->real_escape_string ( $t2 ) ;
	}
}

print get_common_header ( '' , 'Linked items' ) ;

if ( isset ( $_REQUEST['doit'] ) ) {

	$links = array() ;
	$links1 = array() ;

	$db = '' ;
	$wiki = "$language$project" ;
	$wiki = preg_replace ( '/wikipedia$/' , 'wiki' , $wiki ) ;

	if ( $pagepile != '' ) {
		$pp = new PagePile ( $pagepile ) ;

		$wiki = $pp->getWiki() ;
		$db = openDBwiki ( $wiki ) ;
		
		$pp->each ( function ( $o , $pp ) use ( &$links , &$links1 , &$db ) {
			if ( $o->ns != 0 ) return ;
			$t1 = str_replace ( ' ' , '_' , $o->page ) ;
			$t2 = str_replace ( '_' , ' ' , $o->page ) ;
			$links1[$t1] = $db->real_escape_string ( $t1 ) ;
			$links[$t1] = $db->real_escape_string ( $t2 ) ;
		} ) ;
		
		
	} else {
		$db = openDB ( $language , $project ) ;

		$url = "https://$language.$project.org/w/api.php" ;
		$data = array ( 'action' => 'parse' , 'prop' => 'links' , 'format' => 'json' , 'redirects' => 1 ) ;
	
		if ( $page != '' ) {
			$data['page'] = $page ;
			runQuery ( $url , $data ) ;
		} else if ( $pages != '' ) {
			foreach ( explode("\n",$pages) AS $page ) {
				$page = trim ( $page ) ;
				if ( $page == '' ) continue ;
				$data['page'] = $page ;
				runQuery ( $url , $data ) ;
			}
		} else if ( $wikitext != '' ) {
			$data['text'] = $wikitext ;
			runQuery ( $url , $data ) ;
		} else {
			print "<div class='lead'>Gotta gimme a page or wikitext, man!</div>" ;
			print get_common_footer() ;
			exit ( 0 ) ;
		}

	}

	if ( count ( $links ) == 0 ) {
		print "<div class='lead'>No links!</div>" ;
		print get_common_footer() ;
		exit ( 0 ) ;
	}

	
	// resolve redirects
	$redirs = array();
	$sql = "SELECT * FROM page,redirect WHERE page_id=rd_from AND page_namespace=0 AND rd_namespace=0 AND page_title IN (\"" . implode('","',$links1) . '")' ;
	$result = getSQL ( $db , $sql , 2 ) ;
	while($o = $result->fetch_object()){
		if ( isset($redirs[$o->page_title]) ) continue ;
		$redirs[$o->page_title] = 1 ;
		$links[$o->page_title] = $db->real_escape_string ( str_replace ( '_' , ' ' , $o->rd_title ) ) ;
	}
	

	$db = openDB ( 'wikidata' , 'wikidata' ) ;

	
	$l1 = array() ;
	$l2 = array() ;
	$sql = "SELECT * FROM wb_items_per_site WHERE ips_site_id='$wiki' AND ips_site_page IN (\"" . implode ( '","' , $links ) . "\") " ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()){
		$l1[] = 'Q' . $o->ips_item_id ;
		$l2[] = 'Q' . $o->ips_item_id . "|" . $o->ips_site_page ;
	}
	
	print "<div class='lead'>" . count($l1) . " items found</div>" ;
	
	if ( $format == 'pagepile' ) {
	
		$pp = new PagePile ;
		$pp->createNewPile ( 'wikidatawiki' ) ;
		foreach ( $l1 AS $v ) $pp->addPage ( $v , 0 ) ;
		$pp->printAndEnd(false) ;
	
	} else {
		print "<h2>Item list</h2><form method='post' action='//tools.wmflabs.org/autolist/index.php'><textarea id='l1' name='manual_list' style='width:100%' rows=10>" . implode("\n",$l1) . "</textarea><input type='submit' name='run' value='Open in AutoList' class='btn btn-primary'/></form>" ;
		print "<h2>Item and page list</h2><textarea id='l2' style='width:100%' rows=10>" . implode("\n",$l2) . "</textarea>" ;
	}
	
//	print "<pre>" ; print_r ( $links ) ; print "</pre>" ;
	
} else {
	print "<form method='post' class='form form-inline inline-form'>
	<table class='table '><tbody>
	<tr><th>Page</th><td>
	<input type='text' name='language' value='$language' />&nbsp;.&nbsp;
	<input type='text' name='project' value='$project' />, page
	<input type='text' name='page' value='$page' />, <i>or</i>
	</td></tr>
	<tr><th>Pages</th><td><textarea name='pages' style='width:100%' rows=5 placeholder='One page per row, using language/project from above'>$pages</textarea><i>, or</i></td></tr>
	<tr><th>Wikitext</th><td><textarea name='wikitext' style='width:100%' rows=10>$wikitext</textarea>, or</i></td></tr>
	<tr><th>PagePile</th><td><input type='number' name='pagepile' value='$pagepile' placeholder='PagePile ID' /> <small>(instead of wikitext or page above)</small></td></tr>
	<tr><th>Output</th><td>
	<label><input type='radio' name='format' value='screen' ".($format=='screen'?'checked':'')." /> Screen</label>
	<label><input type='radio' name='format' value='pagepile' ".($format=='pagepile'?'checked':'')." /> PagePile</label>
	</tr>
	<tr><td><td colspan='2' style='text-align:right'><input type='submit' name='doit' value='Do it!' class='btn btn-primary' /></td></tr>
	</tbody></table>
	</form>" ;
}

print get_common_footer() ;

?>