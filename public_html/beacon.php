<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // |E_ALL
ini_set('display_errors', 'On');
ini_set('memory_limit','3500M');
set_time_limit ( 60 * 10 ) ; // Seconds

include_once ( 'php/common.php' ) ;

$prop = preg_replace ( '/\D/' , '' , get_request ( 'prop' , '' ) ) ;
$source = preg_replace ( '/\D/' , '' , get_request ( 'source' , '0' ) ) ;
$site = trim ( strtolower ( get_request ( 'site' , '' ) ) ) ;

$db = openDB ( 'wikidata' , 'wikidata' ) ;

// All properties with formatter URL
$targets = array ( '0' => array ( 'Wikidata' , 'http://www.wikidata.org/wiki/' ) ) ;
$sql = "SELECT DISTINCT page_title,term_text FROM pagelinks,page,wb_terms WHERE page_namespace=120 AND page_title=term_full_entity_id and term_entity_type='property' and term_language='en' and term_type='label' and pl_from=page_id and pl_title='P1630' and pl_namespace=120 and pl_from_namespace=120 ORDER BY term_text" ;
$result = getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()){
	$targets[substr($o->page_title,1)] = array ( $o->term_text , '' ) ;
}


// We could do that internally, but...
$l1 = getPagesInCategory ( $db , 'Properties with string-datatype' , 1 , 121 ) ;
$l2 = getPagesInCategory ( $db , 'Properties with unique value constraints' , 1 , 121 ) ;
$others = array() ;
foreach ( $l1 AS $k => $v ) {
	if ( isset($l2[$k]) ) $others[] = $k ;
}

foreach ( $others AS $title ) {
	$title = explode ( ":P" , $title ) ;
	if ( count ( $title ) != 2 ) continue ;
	$p = $title[1] * 1 ;
	if ( isset ( $targets[$p] ) ) continue ;
	
	$targets[$p] = array ( "" , "" ) ;
}

$sql = "select term_full_entity_id,term_text,term_language,term_type from wb_terms where term_entity_type='property' AND term_full_entity_id IN ('Q" . implode("','Q",array_keys($targets)) . "')" ; //  and term_language='en' and term_type='label'
//print "<pre>" ;print_r ( $sql ) ;print "</pre>" ; exit(0);
$result = getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()){
	if ( $o->term_type != 'label' ) continue ;
	if ( $o->term_language != 'en' ) continue ;
	$oq = preg_replace ( '/\D/' , '' , $o->term_full_entity_id ) ;
	if ( $targets[$oq][0] != '' ) continue ;
	$targets[$oq][0] = ucfirst ( $o->term_text ) ;
}


function getSelectHTML ( $name , $use_wikidata ) {
	global $keys ;
	$ret = '' ;
	$ret .= "<select name='$name' class='form-control custom-select'>" ;

	$first = true ;
	if ( $use_wikidata ) {
		$ret .= "<option value='0' selected>Wikidata</option>" ;
		$first = false ;
	}
	foreach ( $keys AS $name => $prop ) {
		if ( $prop == 0 ) continue ;
		$ret .= "<option value='$prop'" ;
		if ( $first ) $ret .= " selected" ;
		$first = false ;
		$ret .= ">$name [P$prop]</option>" ;
	}
	$ret .= "</select>" ;
	return $ret ;
}

function mycmp ( $a , $b ) {
	return strcasecmp ( $a , $b ) ;
}


if ( $prop == '' ) {
	print get_common_header ( '' , "Wikidata BEACON" ) ;
//	print "<pre>" ; print_r ( $targets ) ; print "</pre>" ;
	print "<div class='lead'>This is a <a href='//meta.wikimedia.org/wiki/BEACON' target='_blank'>BEACON</a> generator, mapping Wikidata items to several other IDs. " ;
	print "It uses <a href='https://query.wikidata.org/'>SPARQL</a> as a data source. Data should be no more than a few seconds behind the live site." ;

	$keys = array() ;
	foreach ( $targets AS $k => $v ) $keys[$v[0]] = $k ;
	uksort ( $keys , mycmp ) ;
//if ( isset($_REQUEST['testing']) ) { print "<pre>" ; print_r ( $keys ) ; print "</pre>" ; }
/*
	print "<p><i>Note:</i> You can state a different source than Wikidata, e.g. <a href='?prop=434&source=214'>VIAF&rArr;MusicBrainz</a>; the results will be constructed from Wikidata items that have both. The message field will be the Wikidata ID.</p>" ;
	print "<ul>" ;
	foreach ( $keys AS $name => $prop ) {
		if ( $prop == 0 ) continue ;
		print "<li><a href='?prop=$prop'>$name</a>" ;
		print " <small>(<a href='./translate_items_with_property.php?prop=$prop' target='_blank'>download translations</a>)</small>" ;
		print "</li>" ;
	}
	print "</ul>" ;*/
	
	print "<div class='lead'>
	<form method='get' action='?' class='form form-inline inline-form'>
	<table class='table table-striped'>
	<tr><th>Property</th><td>" ;
	
	print getSelectHTML ( 'prop' , false ) ;
	
	print "</td></tr>
	<tr><th>Source</th><td>" ;
	
	print getSelectHTML ( 'source' , true ) ;
	print "<br/>
	<small>For a source other than Wikidata, the results will be constructed from Wikidata items that have both.<br/>
	In that case, the message field will be the Wikidata ID.</small>
	</td></tr>
	
	<tr><th>Wiki</th>
	<td><input type='text' name='site' value='$site' placeholder='e.g. enwiki' /> <small>only Wikidata items with a link to that wiki; optional</small></td></tr>
	
	<tr><td/><td>
	<input type='submit' class='btn btn-primary' value='Get BEACON data' />
	</td></tr>
	
	</table>
	</form>
	</div>" ;
	
	
	print "<hr/><div class='lead'>
	<form method='get' action='translate_items_with_property.php?' class='form form-inline inline-form'>
	Item translations for property " ;

	print getSelectHTML ( 'prop' , false ) ;
	
	print " <input type='submit' class='btn btn-primary' value='Get translations' />
	</form>
	</div>" ;
	
	print get_common_footer() ;
	exit ( 0 ) ;
}

if ( !isset ( $targets[$prop] ) ) {
	print get_common_header ( '' , "Wikidata BEACON" ) ;
	print "<p>ERROR: Property P$prop is unknown to this service. Ask <a href='mailto:magnusmanske@googlemail.com'>Magnus Manske</a> to add it (give property ID, name, and URL target!).</p>" ;
	print get_common_footer() ;
	exit ( 0 ) ;
}

function setPropertyFormatter ( $p ) {
	global $targets ;
	if ( $p == '' ) return ;
	if ( !isset($targets[$p]) ) return ;
#	$url = "https://www.wikidata.org/wiki/Special:EntityData/P$p.json" ;
	$url = "https://www.wikidata.org/w/api.php?action=wbgetentities&ids=P{$p}&format=json" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	$pp = "P$p" ;
	$p1630 = 'P1630' ;
	if ( !isset($j->entities->$pp) ) return ;
	if ( !isset($j->entities->$pp->claims->$p1630) ) return ;
	$a = $j->entities->$pp->claims->$p1630 ;
	$formatter = $a[0]->mainsnak->datavalue->value ;
	if ( !preg_match ( '/^(.+)\$1\/*$/' , $formatter , $m ) ) return ;
	$targets[$p][1] = $m[1] ;
}


function filterBySite ( &$items ) {
	global $site , $db ;
	if ( $site == '' ) return ;
	if ( count($items) == 0 ) return ;
	$sql = "SELECT DISTINCT ips_item_id FROM wb_items_per_site WHERE ips_site_id='" . $db->real_escape_string($site) . "' AND ips_item_id IN (" . implode(',',$items) . ")" ;
	$result = getSQL ( $db , $sql ) ;
	$items = array() ;
	while($o = $result->fetch_object()) $items[] = $o->ips_item_id * 1 ;
}


header('Content-type: text/plain; charset=UTF-8');


// TODO filter SPARQL by site

$sparql = '' ;
if ( $source == '0' ) { // Just external-to-WD
	$sparql = 'SELECT DISTINCT ?item ?ext1 WHERE { ?item wdt:P'.$prop.' ?ext1 FILTER ( bound(?ext1) ) } ORDER BY ?ext1' ;
} else { // external1-WD-external2
	$sparql = 'SELECT DISTINCT ?item ?ext1 ?ext2 WHERE { ?item wdt:P'.$prop.' ?ext1 . ?item wdt:P'.$source.' ?ext2 FILTER ( bound(?ext1) ) FILTER ( bound(?ext2) ) } ORDER BY ?ext2' ;
}
$j = getSPARQL ( $sparql ) ;

// Subset items for site
$items = array() ;
if ( $site != '' ) {
	foreach ( $j->results->bindings AS $x ) {
		$items[] = preg_replace ( '/^.+entity\/Q/' , '' , $x->item->value ) ;
	}
	filterBySite ( $items ) ;
}
//print "<pre>" ; print_r ( $j ) ; print "</pre>" ; exit(0) ;

setPropertyFormatter ( $source ) ;
setPropertyFormatter ( $prop ) ;

$my_url = "http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"] ;

print "#NAME: " . $targets[$prop][0] . "\n" ;
print "#DESCRIPTION: Mapping from " . $targets[$source][0] . " IDs to " . $targets[$prop][0] . " IDs\n" ;
print "#CREATOR: Magnus Manske\n" ;
print "#CREATOR: https://www.wikidata.org/wiki/User:Magnus_Manske\n" ;
print "#HOMEPAGE: $my_url\n" ;
print "#TIMESTAMP: " . date('Y-m-d\TH:i:sP') . "\n" ;
if ( $targets[$source][1] != '' ) print "#PREFIX: " . $targets[$source][1] . "\n" ;
if ( $targets[$prop][1] != '' ) print "#TARGET: " . $targets[$prop][1] . "\n" ;
print "\n" ;

foreach ( $j->results->bindings AS $x ) {
	$q = preg_replace ( '/^.+entity\/Q/' , '' , $x->item->value ) * 1 ;
	if ( $site != '' ) {
		if ( !in_array ( $q , $items ) ) continue ;
	}
	$ext1 = $x->ext1->value ;
	if ( $ext1 == 'novalue' ) continue ;
	
	if ( $source == 0 ) {
		print "Q$q||$ext1\n" ;
	} else {
		$ext2 = $x->ext2->value ;
		if ( $ext2 == 'novalue' ) continue ;
		print "$ext2|Q$q|$ext1\n" ;
	}
}

/*
if ( $source == 0 ) {
	foreach ( $wdq->props->$prop AS $v ) {
		if ( !in_array ( $v[0] , $wdq->items ) ) continue ;
		print "Q".$v[0]."||".$v[2]."\n" ;
	}
} else {
	$tmp = array() ;
	foreach ( $wdq->props->$prop AS $v ) {
		$tmp[$v[0]] = $v[2] ;
	}
	foreach ( $wdq->props->$source AS $v ) {
		if ( !in_array ( $v[0] , $wdq->items ) ) continue ;
		print $v[2]."|Q".$v[0]."|".$tmp[$v[0]]."\n" ;
	}
}
*/

?>