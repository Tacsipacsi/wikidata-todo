<?PHP

require_once ( 'php/common.php' ) ;

$keyprop = get_request ( 'keyprop' , '' ) ;
$values = get_request ( 'values' , '' ) ;
$columns = get_request ( 'columns' , 'Q' ) ;

print get_common_header ( '' , 'Multi-BEACON' ) ;

print "<div class='lead'>Give a (string/ID) property and list of values, and generate a table of values for other properties (use \"Q\" for the Wikidata item).</div>" ;
print "<div><form class='form form-inline inline-form' method='post'>
<h3>Key property</h3>
<p><input type='text' name='keyprop' value='$keyprop' /></p>
<h3>Values for key property</h3>
<p><textarea style='width:100%' rows='5' name='values'>$values</textarea></p>
<p>One per line</p>

<h3>Columns</h3>
<p><textarea style='width:100%' rows='3' name='columns'>$columns</textarea></p>
<p>One per line; Q=wikidata item ID. Key property will automatically become first column.</p>

<p><input type='submit' name='doit' value='Do it!' class='btn btn-primary' /></p>
</form></div>" ;

if ( $keyprop != '' and $columns != '' ) {
	$keyprop = preg_replace ( '/\D/' , '' , $keyprop ) ;

	$columns = explode ( "\n" , $columns ) ;
	$columns2 = array() ;
	$columns2[] = array ( "P$keyprop" , $keyprop ) ;
	$props = array($keyprop) ;
	$prop2col = array() ;
	foreach ( $columns AS $c ) {
		$c = trim($c) ;
		if ( preg_match ( '/^[pP]{0,1}(\d+)$/' , $c , $m ) ) {
			$props[] = $m[1] ;
			$prop2col[$m[1]] = count($columns2) ;
			$columns2[] = array ( $c , $m[1] ) ;
		} else if ( strtolower($c) == 'q' ) {
			$prop2col['Q'] = count($columns2) ;
			$columns2[] = array ( $c , 'Q' ) ;
		}
	}

	$header = array() ;
	foreach ( $columns2 AS $c ) $header[] = $c[0] ;
	
	$out = array() ;
	$out[] = implode ( "\t" , $header ) ;
	$values = explode ( "\n" , $values ) ;
	foreach ( $values AS $v ) {
		$v = trim ( $v ) ;
		$url = "$wdq_internal_url?q=" . urlencode ( 'string[' . $keyprop . ":" . json_encode($v) . "]" ) . "&props=" . implode ( "," , $props ) ;
#		print "<pre>$url</pre>" ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		
		$data = array() ;
		foreach ( $j->props AS $p => $arr ) {
			foreach ( $arr AS $x ) $data["$p"][''.$x[0]] = $x[2] ;
		}
		
		$cnt = 0 ;
		foreach ( $j->items AS $q ) {
			$o = array() ;
			foreach ( $columns2 AS $c ) {
				if ( $c[1] == 'Q' ) {
					$o[] = "Q$q" ;
				} else {
					if ( isset($data[$c[1]][$q]) ) $o[] = $data[$c[1]][$q] ;
					else $o[] = '' ;
				}
			}
			$out[] = implode ( "\t" , $o ) ;
			$cnt++ ;
		}
		if ( $cnt == 0 ) $out[] = $v ;
		
	}
	
	print "<h3>Results</h3>" ;
	print "<textarea style='width:100%' rows=10>" ;
	print implode ( "\n" , $out ) ;
	print "</textarea>" ;
	
#	print "<pre>" ; print_r ( $columns2 ) ; print "</pre>" ;
}

print get_common_footer() ;

?>