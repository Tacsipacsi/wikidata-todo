function toggleHowto() {
    var div = document.getElementById('howto');
    if (div.style.display !== 'none') div.style.display = 'none';
    else div.style.display = 'block';
}

function getParams ( q , p , v ) {
    if ( !q.match(/^P\d+/) )  q = 'Q'+(''+q).replace(/\D/g,'') ;
    p = 'P'+(''+p).replace(/\D/g,'') ;
    var params ;
    var m = v.match ( /^"(.+)"$/ ) ;
    if ( m != null ) {
        return { botmode:1 , action:'set_string' , id:q , prop:p , text:m[1] } ;
    }

    var m = v.match ( /^([a-zA-Z-_]+):"(.+)"$/ ) ;
    if ( m != null ) {
        return { botmode:1 , action:'set_monolang' , id:q , prop:p , language:m[1] , text:m[2] } ;
    }

    m = v.match ( /^\s*(Q\d+)\s*/i ) ;
    if ( m != null ) {
        return { botmode:1 , action:'set_claims' , ids:q , prop:p , target:m[1] } ;
    }

    m = v.match ( /^\s*@(-{0,1}[0-9.]+)\/(-{0,1}[0-9.]+)\s*/i ) ;
    if ( m != null ) {
        return { botmode:1 , action:'set_location' , id:q , prop:p , lat:m[1] , lon:m[2] } ;
    }

    m = v.match ( /^\s*([+-]\S+T\S+Z\S*)\s*/i ) ;
    if ( m != null ) {
        d = m[1].split('/') ;
        if ( d.length == 1 ) d.push('9') ; // Precision appended with "/"; default:year
        return { botmode:1 , action:'set_date' , id:q , prop:p , date:d[0] , prec:d[1] } ;
    }

    /*
     * <Quantity> ::= <Amount> [ '[' <Lower> ',' <Upper> ']' ] <Unit>
     * <Amount> ::= <Decimal>
     * <Lower> ::= <Decimal>
     * <Upper> ::= <Decimal>
     * <Unit> ::= 'U' <Digits>
     * <Decimal> ::= <Digits> [ '.' <Digits> ]
     * <Digits> ::= '0' - '9'
     *
     * Eg. 10, 10.2, 10[8,12], 0[-1,1], -2[-4,0], 10.2[8.2,12.2], 10U123, 10[9.5,11.5]U123…
     */
    m = v.match ( /^\s*([+-]?\d+(?:\.\d+)?)(?:\[([+-]?\d+(?:\.\d+)?),([+-]?\d+(?:\.\d+)?)])?(?:U(\d+))?\s*/i ) ;
    if ( m != null ) {
        var upper, lower, unit ;
        // Enforce string to avoid precision issues.
        var amount = String(m[1]) ;
        // Upper and lower bounds
        if ( m[2] === undefined || m[3] === undefined ) {
            // Bounds are not set
            upper = amount ;
            lower = amount ;
        } else {
            upper = String(m[2]) ;
            lower = String(m[3]) ;
        }
        // Unit
        if ( m[4] === undefined ) {
            // '1' means no unit
            unit = '1';
        } else {
            // Must use full URI
            unit = 'http://www.wikidata.org/entity/Q' + m[4] ;
        }
        return { botmode:1 , action:'set_quantity' , id:q , prop:p , amount:amount, upper:upper, lower:lower, unit:unit } ;
    }

    return params ;
}

function getSourceParams ( q , p , v ) {
    q = 'Q'+(''+q).replace(/\D/g,'') ;
    p = 'P'+(''+p).replace(/\D/g,'') ;
    var params ;
    var m = v.match ( /^"(.+)"$/ ) ;
    if ( m != null ) {
        var ret = {} ;
        ret[p] = [{
            snaktype:"value",
            property:p,
            datavalue:{
                type:"string",
                value:m[1]
            }
        }] ;
        return ret ;
    }

    m = v.match ( /^\s*(Q\d+)\s*/i ) ;
    if ( m != null ) {
        var ret = {} ;
        ret[p] = [{
            snaktype:"value",
            property:p,
            datavalue:{
                type:"wikibase-entityid",
                value:{
                    "entity-type": "item",
                    "numeric-id":m[1].replace(/\D/g,'')*1
                }
            }
        }
        ] ;
        return ret ;
    }

    m = v.match ( /^\s*([+-]0\S+)\s*/i ) ;
    if ( m != null ) { // TODO
        d = m[1].split('/') ;
//		if ( d.length == 1 ) d.push('9') ; // Precision appended with "/"; default:year
//		return { botmode:1 , action:'set_date' , id:q , prop:p , date:d[0] , prec:d[1] } ;
    }

    return null ;
}

var max_concurrent = 1 ;
var running = 0 ;
var widar_api = '/widar/index.php' ;

function getWidar ( params , callback ) {
    params.tool_hashtag = 'quickstatements' ;
    $.get ( widar_api , params , function ( d ) {
        if ( d.error != 'OK' ) {
            console.log ( params ) ;
            if ( null != d.error.match(/Invalid token/) || null != d.error.match(/happen/) || null != d.error.match(/Problem creating item/) || ( params.action!='create_redirect' && null != d.error.match(/failed/) ) ) {
                console.log ( "ERROR (re-trying)" , params , d ) ;
                setTimeout ( function () { getWidar ( params , callback ) } , 500 ) ; // Again
            } else {
                console.log ( "ERROR (aborting)" , params , d ) ;
                var h = "<li style='color:red'>ERROR (" + params.action + ") : " + d.error + "</li>" ;
                $('#out ol').append(h) ;
                callback ( d ) ; // Continue anyway
            }
        } else {
            callback ( d ) ;
        }
    } , 'json' ) . fail(function() {
        console.log ( "Again" , params ) ;
        getWidar ( params , callback ) ;
    }) ;
}

function addQualifiers ( row , claim ) {

    if ( row.length == 3 ) {
        running-- ;
        doNextOne() ;
        return ;
    }

    var val = row.pop() ;
    var p = row.pop() ;


    if ( null != p.match ( /^S\d+/ ) ) {

        p = 'P'+(''+p).replace(/\D/g,'') ;
        var statement = claim.id ;

        var snaks = getSourceParams ( row[0] , p , val ) ;

        if ( snaks == null ) {
            addQualifiers ( row , claim ) ;
            return ;
        }


        function doThat1 () {
            getWidar ( {
                action:'add_source',
                statement:statement,
                snaks:JSON.stringify(snaks),
                botmode:1
            } , function ( d2 ) {
                addQualifiers ( row , claim ) ;
            } ) ;
        }

        doThat1() ;


    } else {
        p = 'P'+(''+p).replace(/\D/g,'') ;

        var exists = false ;
        $.each ( (claim.qualifiers||[]) , function ( k , v ) {
            if ( k == p ) exists = true ;
        } ) ;
        if ( exists ) {
            addQualifiers ( row , claim ) ; // Next one
            return ;
        }

        var params2 = getParams ( row[0] , p , val ) ;
        params2.claim = claim.id ;

        function doThat2() {
            getWidar ( params2 , function ( d2 ) {
                addQualifiers ( row , claim ) ;
            } ) ;
        }

        doThat2() ;

    }
}

function setLabel ( q , lang , value ) {
    getWidar ( {
        action : 'set_label' ,
        q : (q+'').replace('/\D/g','') ,
        label : value.replace(/^"/,'').replace(/"$/,'') ,
        lang : lang ,
        botmode : 1
    } , function ( d ) {
//		console.log ( d ) ;
        running-- ;
        doNextOne() ;
    } ) ;
}

function setDesc ( q , lang , value ) {
    getWidar ( {
        action : 'set_desc' ,
        q : (q+'').replace('/\D/g','') ,
        label : value.replace(/^"/,'').replace(/"$/,'') ,
        lang : lang ,
        botmode : 1
    } , function ( d ) {
//		console.log ( d ) ;
        running-- ;
        doNextOne() ;
    } ) ;
}

function set_Alias ( q , lang , value ) {
    getWidar ( {
        action : 'set_alias' ,
        q : (q+'').replace('/\D/g','') ,
        label : value.replace(/^"/,'').replace(/"$/,'') ,
        lang : lang ,
        mode : 'add' ,
        botmode : 1
    } , function ( d ) {
//		console.log ( d ) ;
        running-- ;
        doNextOne() ;
    } ) ;
}

// https://tools.wmflabs.org/widar/index.php?action=set_sitelink&q=Q6474469&site=eowiki&title=Lajos+Tak%C3%A1cs
function set_Site ( q , site , value ) {
//	console.log ( (q+'').replace('/\D/g','') , site , value ) ;
    getWidar ( {
        action : 'set_sitelink' ,
        q : (q+'').replace('/\D/g','') ,
        title : value.replace(/^"/,'').replace(/"$/,'') ,
        site : site ,
        botmode : 1
    } , function ( d ) {
//		console.log ( d ) ;
        running-- ;
        doNextOne() ;
    } ) ;
}



var last_q = '' ;

function createNewItem () {
    getWidar ( {
        action:'create_blank_item',
        botmode:1
    } , function ( d ) {
        if ( typeof d.q == 'undefined' ) {
            createNewItem() ;
            return ;
        }
        last_q = d.q ;
        running-- ;
        doNextOne() ;
    } ) ;
}



function redirectItem ( from , to ) {
    getWidar ( {
        action:'create_redirect',
        from:'Q'+(''+from).replace(/\D/g,''),
        to:'Q'+(''+to).replace(/\D/g,''),
        botmode:1
    } , function ( d ) {
//		console.log ( d ) ;
        var h = "<li>Merged " + from + " into " + to + ".</li>" ;
        $('#out ol').append(h) ;
        running-- ;
        doNextOne() ;
    } ) ;
}


function mergeItems ( from , to ) {
    getWidar ( {
        action:'merge_items',
        from:'Q'+(''+from).replace(/\D/g,''),
        to:'Q'+(''+to).replace(/\D/g,''),
        botmode:1
    } , function ( d ) {
        if ( d.error != 'OK' ) {
            var h = "<li>Error while merging " + from + " into " + to + ": " + d.error + "</li>" ;
            $('#out ol').append(h) ;
            running-- ;
            doNextOne() ;
            return ;
        }
        redirectItem ( from , to ) ;
    } ) ;
}


function doNextOne() {
//	console.log ( "NEXTONE" , rows.length , running ) ;
    if ( rows.length == 0 ) {
        if ( running == 0 ) {
            var h = "<hr/><div>All done!.</div>" ;
            $('#out').append(h) ;
        }
        return ;
    }
    if ( running >= max_concurrent ) return ;
    running++ ;

    var row = $.trim(rows.shift()) ;

    if ( row == '' ) {
        running-- ;
        doNextOne() ;
        return ;
    }

    if ( row.toLowerCase() == 'create' ) {
        createNewItem() ;
        return ;
    }

    row = row.split("\t") ;

    if ( row[0].toUpperCase() == 'MERGE' ) {
        mergeItems ( row[1] , row[2] ) ;
        return ;
    }


    if ( row[0] == 'MISSING' ) {
        var h = "<li>Skipping an item that could not be found.</li>" ;
        $('#out ol').append(h) ;
        running-- ;
        doNextOne() ;
        return ;
    }


    if ( $.trim(row[0]).toLowerCase() == 'last' ) {
        if ( last_q == '' ) {
            var h = "<li>No LAST item defined!.</li>" ;
            $('#out ol').append(h) ;
            running-- ;
            doNextOne() ;
            return ;
        }
        console.log ( "Replacing LAST with " + last_q ) ;
        row[0] = last_q ;
    }

    if ( $.trim(row[2]).toLowerCase() == 'last' ) {
        if ( last_q == '' ) {
            var h = "<li>No LAST item defined!.</li>" ;
            $('#out ol').append(h) ;
            running-- ;
            doNextOne() ;
            return ;
        }
        console.log ( "Replacing LAST with " + last_q ) ;
        row[2] = last_q ;
    }

    if ( null == row[0].match(/^[PQ]\d+/) ) {
        var h = "<li>ERROR: " + row[0] + " is not a Wikidata item (Qxxx). Did you forget to set a wiki to convert from articles to items?</li>" ;
        $('#out ol').append(h) ;
        running-- ;
        doNextOne() ;
        return ;
    }

    last_q = row[0] ;

    var h = "<li>Processing <a href='https://www.wikidata.org/wiki/" + row[0] + "' target='_blank'>" + row[0] + "</a> (" + row.join(" ") + ")</li>" ;
    $('#out ol').append(h) ;

    var m = row[1].match ( /^L(.+)$/ ) ;
    if ( m != null ) {
        setLabel ( row[0] , m[1].toLowerCase() , row[2] ) ;
        return ;
    }

    var m = row[1].match ( /^D(.+)$/ ) ;
    if ( m != null ) {
        setDesc ( row[0] , m[1].toLowerCase() , row[2] ) ;
        return ;
    }

    var m = row[1].match ( /^A(.+)$/ ) ;
    if ( m != null ) {
        set_Alias ( row[0] , m[1].toLowerCase() , row[2] ) ;
        return ;
    }

    var m = row[1].match ( /^S(.+)$/ ) ;
    if ( m != null ) {
        set_Site ( row[0] , m[1].toLowerCase() , row[2] ) ;
        return ;
    }


    var params = getParams ( row[0] , row[1] , row[2] ) ;
    if ( typeof params == 'undefined' ) {
        console.log ( "ERROR" , row ) ;
        running-- ;
        doNextOne() ;
        return ;
    }

    function doMain() {
//console.log(params);
        getWidar ( params , function ( d ) {
//console.log(d);
            if ( row.length == 3 ) { // No qualifiers
                running-- ;
                doNextOne() ;
                return ;
            }

            var q = params.id||params.ids ;
            var claim = (d.res||{}).claim||{} ;

            if ( typeof claim.id == 'undefined' ) {
                $.getJSON ( 'https://www.wikidata.org/w/api.php?callback=?' , {
                    action:'wbgetentities',
                    ids:q,
                    format:'json'
                } , function ( d2 ) {
                    var p = params.prop ;

                    var found = false ;
                    $.each ( ((((d2.entities[q])||{}).claims||{})[p]||[]) , function ( k , v ) {
                        var type = v.mainsnak.datatype ;
                        if ( type == 'wikibase-item' ) {
                            if ( 'Q'+v.mainsnak.datavalue.value['numeric-id'] != params.target ) return ;
                        } else if ( type == 'string' ) {
                            if ( v.mainsnak.datavalue.value != params.text ) return ;
                        } else if ( type == 'time' ) {
                            var t1 = v.mainsnak.datavalue.value.time ;
                            var t2 = params.date ;
                            t1 = t1.replace ( /^(.)0+/ , '$1' ) ;
                            t2 = t2.replace ( /^(.)0+/ , '$1' ) ;
//							console.log ( t1 , t2 ) ; return ;
                            if ( t1 != t2 ) return ;
                        } else if ( type == 'globe-coordinate' ) {
                            var factor = 1000000000000 ;
                            var lat = Math.round(params.lat*factor)/factor ;
                            var lon = Math.round(params.lon*factor)/factor ;
//							console.log ( v.mainsnak.datavalue.value ) ;
//							console.log ( lat,lon ) ;
                            if ( v.mainsnak.datavalue.value.latitude != lat || v.mainsnak.datavalue.value.longitude != lon ) return ;
                        } else if ( type == 'monolingualtext' ) {
                            if ( v.mainsnak.datavalue.value['text'] != params.text || v.mainsnak.datavalue.value['language'] != params.language ) return ;
                        } else if ( type == 'quantity' ) {
                            if ( v.mainsnak.datavalue.value['amount'] != params.amount || v.mainsnak.datavalue.value['unit'] != 'http://www.wikidata.org/entity/' + params.unit ) return ;
                        } else { // Unknown type
                            console.log ( "UNKNOWN TYPE " + type ) ;
                            console.log ( v.mainsnak.datavalue.value ) ;
                            console.log ( params ) ;
                            return ;
                        }
                        found = true ;
                        claim = v ;
                        addQualifiers ( row , claim ) ;
                        return false ;
                        //					console.log ( "EXISTING" , v ) ;
                    } ) ;
                    if ( !found ) {
                        running-- ;
                        doNextOne() ;
                    }

                } ) ;
            } else {
                addQualifiers ( row , claim ) ;
            }


        } ) ;
    }

    doMain() ;
}
