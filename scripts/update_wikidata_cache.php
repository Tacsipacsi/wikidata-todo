#!/usr/bin/php
<?PHP

exit(0) ; // DOnt use this

require_once ( '/data/project/wikidata-todo/public_html/php/common.php' ) ;


function getChangedItemsList ( $since ) {
	$db_wd = openDB ( 'wikidata' , 'wikidata' ) ;
	$ret = array() ;
	$sql = "select page_title,max(rev_timestamp) AS ts from page,revision where page_namespace=0 and page_latest=rev_id and page_id=rev_page and rev_timestamp>='$since' GROUP BY page_title" ;
	$db_wd->ping() ;
	if(!$result = $db_wd->query($sql)) die('There was an error running the query [' . $db_wd->error . ']'."\n$sql\n\n");
	while($o = $result->fetch_object()){
		$ret[$o->page_title] = $o->ts ;
	}
	$db_wd->close() ;
	return $ret ;
}

$since = '20141209162700' ; // Dummy default; was only used to init

while ( 1 ) {
	$sql = "SELECT max(timestamp) AS since FROM json_cache" ;
	$db = openToolDB ( 'wikidata_cache_p' ) ;
	$db->ping() ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
	while($o = $result->fetch_object()) {
		if ( $o->since != null ) $since = $o->since ;
	}
	$items = getChangedItemsList ( $since ) ;
	foreach ( $items AS $q => $ts ) {
		$json = file_get_contents ( "http://www.wikidata.org/wiki/Special:EntityData/$q?format=json" ) ;
		$qn = preg_replace ( '/\D/' , '' , $q ) ;
		$sql = "DELETE FROM json_cache WHERE q=$qn" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
	
		$sql = "INSERT INTO json_cache (q,timestamp,json) VALUES ($qn,'$ts','" . $db->real_escape_string($json) . "')" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
	}
	$db->close() ;
	sleep ( 5 ) ;
}

?>