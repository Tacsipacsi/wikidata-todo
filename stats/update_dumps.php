#!/usr/bin/php
<?PHP

set_time_limit(0);
error_reporting (E_ERROR | E_WARNING | E_PARSE) ;

function downloadFile ( $url , $target ) {
	$cmd = "curl -s -o $target $url" ;
	print "$cmd\n" ;
	exec ( $cmd ) ;
}

function dc ( $d ) {
	return substr($d,0,4) . '-' . substr($d,4,2) . '-' . substr($d,6,2) ;
}

chdir ( '/data/project/wikidata-todo/stats' ) ;

$dump_dir = '/shared/dumps' ;
$json_dir = '/data/project/wikidata-todo/stats/data' ;
$base_url = 'https://dumps.wikimedia.org/other/wikidata' ;

// Get available dumps from FTP site
$ftp = file_get_contents ( $base_url ) ;
$ftp = preg_replace ( '/\s+/' , ' ' , $ftp ) ;
preg_match_all ( '/<a href="(\d+.json.gz)">(\d+).json.gz<\/a>\s+\d+-.+?-\d+\s+\d+:\d+\s+(\d+)/' , $ftp , $m ) ;

$last_date = '' ;
$exists = scandir ( $json_dir , SCANDIR_SORT_ASCENDING ) ;
foreach ( $exists AS $v ) {
#	print "$v\n" ;
	$last_date = dc ( $v ) ;
}

foreach ( $m[1] AS $k => $file ) {
	$date = $m[2][$k] ;
	$date2 = dc ( $date ) ;
	$size = $m[3][$k] * 1 ;
	if ( $size < 1000000 ) continue ; // Yes, the WMF deems it amusing to offer non-existant files for download
	if ( $last_date >= $date2 ) continue ;
	
	$datetime1 = new DateTime($last_date);
	$datetime2 = new DateTime($date2);
	$interval = $datetime1->diff($datetime2);
	$diff = $interval->format('%a') * 1 ;
#	print "$last_date\t$date2\t$diff\n" ;
	if ( $diff < 13 ) continue ;
	
	$last_date = $date2 ;
	$url = "$base_url/$file" ;
	$target = "$dump_dir/$file" ;
	if ( file_exists ( $target ) ) continue ;
#	print "LOADING $date2\n" ;
	downloadFile ( $url , $target ) ;
}

exec ( "./wrapper.pl" ) ;

?>
