#!/bin/sh

zcat $2 | /data/project/wikidata-todo/stats/stats_references.php > /data/project/wikidata-todo/stats/data/$1.json.tmp
mv /data/project/wikidata-todo/stats/data/$1.json.tmp /data/project/wikidata-todo/stats/data/$1.json
